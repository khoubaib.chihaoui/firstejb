package mavensayhelloclient;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import sayhellopackage.SayhelloRemote;

public class Test {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		InitialContext ctx=new InitialContext();
        Object obj = ctx.lookup("/mavensayhelloproject/Sayhello!sayhellopackage.SayhelloRemote");
        SayhelloRemote proxy = (SayhelloRemote) obj;
        System.out.println(proxy.sayHello("4infoB2"));
	}

}
