package mavensayhelloclient;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import sayhellopackage.SayhelloRemote;
public abstract class Testclient {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		Hashtable ht=new Hashtable();
		ht.put("java.naming.factory.initial", "org.jboss.naming.remote.client.InitialContextFactory");
		ht.put("java.naming.provider.url","http-remoting://localhost:18080");
		ht.put("jboss.naming.client.ejb.context",true);
 
		Context ctx=new InitialContext(ht);
        Object obj = ctx.lookup("mavensayhelloproject/Sayhello!sayhellopackage.SayhelloRemote");
        SayhelloRemote proxy = (SayhelloRemote) obj;
        System.out.println(proxy.sayHello("4infoB2"));
	}

}
