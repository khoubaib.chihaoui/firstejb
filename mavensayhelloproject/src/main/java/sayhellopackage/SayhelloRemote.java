package sayhellopackage;

import javax.ejb.Remote;

@Remote
public interface SayhelloRemote {
	public String sayHello(String name);

}
