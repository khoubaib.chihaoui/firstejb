package sayhellopackage;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class Sayhello
 */
@Stateless
@LocalBean
public class Sayhello implements SayhelloRemote, SayhelloLocal {

    /**
     * Default constructor. 
     */
    public Sayhello() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public String sayHello(String name) {
		// TODO Auto-generated method stub
		return ("Bonjour "+name);
	}

}
